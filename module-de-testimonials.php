<?php
/*

Plugin Name: Module de testimonials
Version: 1.0
Description: Ce plugin permet aux utilisateurs de suggérer des citations
Author: Ayoub Lahrir
License: MIT

*/


if (!class_exists('WPModuleDeTestimonials')) {
    class WPModuleDeTestimonials
    {
        function __construct()
        {
            add_action('init', array($this, 'createWpTestimonialCpt'), 8);
            add_action('wp_enqueue_scripts', array($this, 'enqueueAssets'));
            add_action('wp_footer', array($this, 'enqueueJs'));
            add_shortcode('testimo_mod_form', [$this, 'shortcode']);
            add_shortcode('testimo_mod_content', [$this, 'shortcodeContent']);
        }
        public function enqueueAssets()
        {
            wp_enqueue_style('module_testimo_style', plugins_url('assets/css/style.css', __FILE__));
            
        }
        public function enqueueJs()
        {
            wp_register_script('module_testimo_js', plugins_url('assets/js/jquery.min.js', __FILE__), array('jquery'), '', true);
            wp_enqueue_script('module_testimo_js');
            wp_register_script('module_testimo_custom_js', plugins_url('assets/js/custom.js', __FILE__), array('jquery'), '', true);
            wp_enqueue_script('module_testimo_custom_js');
        }
        function shortcode()
        {
            ob_start();
            require 'templates/form_submition.php';
            $content = ob_get_clean();
            return $content;
        }
        function shortcodeContent()
        {
            ob_start();
            require 'templates/testimo_grid.php';
            $content = ob_get_clean();
            return $content;
        }

        // Register Custom Post Type Wp Testimonial
        // Post Type Key: Wp Testimonial
        public function createWpTestimonialCpt()
        {

            $labels = array(
                'name' => _x('Wp Testimonials', 'Post Type General Name', 'textdomain'),
                'singular_name' => _x('Wp Testimonial', 'Post Type Singular Name', 'textdomain'),
                'menu_name' => _x('Wp Testimonials', 'Admin Menu text', 'textdomain'),
                'name_admin_bar' => _x('Wp Testimonial', 'Add New on Toolbar', 'textdomain'),
                'archives' => __('Wp Testimonial', 'textdomain'),
                'attributes' => __('Wp Testimonial', 'textdomain'),
                'parent_item_colon' => __('Wp Testimonial', 'textdomain'),
                'all_items' => __('All Wp Testimonials', 'textdomain'),
                'add_new_item' => __('Add New Wp Testimonial', 'textdomain'),
                'add_new' => __('Add New', 'textdomain'),
                'new_item' => __('New Wp Testimonial', 'textdomain'),
                'edit_item' => __('Edit Wp Testimonial', 'textdomain'),
                'update_item' => __('Update Wp Testimonial', 'textdomain'),
                'view_item' => __('View Wp Testimonial', 'textdomain'),
                'view_items' => __('View Wp Testimonials', 'textdomain'),
                'search_items' => __('Search Wp Testimonial', 'textdomain'),
                'not_found' => __('Not found', 'textdomain'),
                'not_found_in_trash' => __('Not found in Trash', 'textdomain'),
                'featured_image' => __('Featured Image', 'textdomain'),
                'set_featured_image' => __('Set featured image', 'textdomain'),
                'remove_featured_image' => __('Remove featured image', 'textdomain'),
                'use_featured_image' => __('Use as featured image', 'textdomain'),
                'insert_into_item' => __('Insert into Wp Testimonial', 'textdomain'),
                'uploaded_to_this_item' => __('Uploaded to this Wp Testimonial', 'textdomain'),
                'items_list' => __('Wp Testimonials list', 'textdomain'),
                'items_list_navigation' => __('Wp Testimonials list navigation', 'textdomain'),
                'filter_items_list' => __('Filter Wp Testimonials list', 'textdomain'),
            );

            $args = array(
                'label' => __('Wp Testimonial', 'textdomain'),
                'description' => __('Module De Testimonials quiz', 'textdomain'),
                'labels' => $labels,
                'menu_icon' => 'dashicons-admin-appearance',
                'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
                'taxonomies' => array(),
                'hierarchical' => false,
                'exclude_from_search' => false,
                'publicly_queryable' => true,
                'has_archive' => true,
                'public' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'show_in_admin_bar' => true,
                'can_export' => true,
                'show_in_nav_menus' => true,
                'menu_position' => 5,
                'capability_type' => 'post',
                'show_in_rest' => true,
            );

            register_post_type('moduledetestimonials', $args);
        }
    }

    new WPModuleDeTestimonials;
}
if (!class_exists('Validation')) {
    class Validation
    {
        private $config = '{"title":"Validation","prefix":"validation_","domain":"validation","class_name":"Validation","post-type":["post"],"context":"normal","priority":"default","cpt":"moduledetestimonials","fields":[{"type":"checkbox","label":"Statue","id":"validation_statue"}]}';

        public function __construct()
        {
            $this->config = json_decode($this->config, true);
            $this->process_cpts();
            add_action('add_meta_boxes', [$this, 'add_meta_boxes']);
            add_action('admin_head', [$this, 'admin_head']);
            add_action('save_post', [$this, 'save_post']);
        }

        public function process_cpts()
        {
            if (!empty($this->config['cpt'])) {
                if (empty($this->config['post-type'])) {
                    $this->config['post-type'] = [];
                }
                $parts = explode(',', $this->config['cpt']);
                $parts = array_map('trim', $parts);
                $this->config['post-type'] = array_merge($this->config['post-type'], $parts);
            }
        }

        public function add_meta_boxes()
        {
            foreach ($this->config['post-type'] as $screen) {
                add_meta_box(
                    sanitize_title($this->config['title']),
                    $this->config['title'],
                    [$this, 'add_meta_box_callback'],
                    $screen,
                    $this->config['context'],
                    $this->config['priority']
                );
            }
        }

        public function admin_head()
        {
            global $typenow;
            if (in_array($typenow, $this->config['post-type'])) {
?><?php
                        }
                    }

                    public function save_post($post_id)
                    {
                        foreach ($this->config['fields'] as $field) {
                            switch ($field['type']) {
                                case 'checkbox':
                                    update_post_meta($post_id, $field['id'], isset($_POST[$field['id']]) ? $_POST[$field['id']] : '');
                                    break;
                                default:
                                    if (isset($_POST[$field['id']])) {
                                        $sanitized = sanitize_text_field($_POST[$field['id']]);
                                        update_post_meta($post_id, $field['id'], $sanitized);
                                    }
                            }
                        }
                    }

                    public function add_meta_box_callback()
                    {
                        $this->fields_table();
                    }

                    private function fields_table()
                    {
                            ?><table class="form-table" role="presentation">
    <tbody><?php
                        foreach ($this->config['fields'] as $field) {
            ?><tr>
                <th scope="row"><?php $this->label($field); ?></th>
                <td><?php $this->field($field); ?></td>
            </tr><?php
                        }
                    ?></tbody>
</table><?php
                    }

                    private function label($field)
                    {
                        switch ($field['type']) {
                            default:
                                printf(
                                    '<label class="" for="%s">%s</label>',
                                    $field['id'],
                                    $field['label']
                                );
                        }
                    }

                    private function field($field)
                    {
                        switch ($field['type']) {
                            case 'checkbox':
                                $this->checkbox($field);
                                break;
                            default:
                                $this->input($field);
                        }
                    }

                    private function checkbox($field)
                    {
                        printf(
                            '<label class="rwp-checkbox-label"><input %s id="%s" name="%s" type="checkbox"> %s</label>',
                            $this->checked($field),
                            $field['id'],
                            $field['id'],
                            isset($field['description']) ? $field['description'] : ''
                        );
                    }

                    private function input($field)
                    {
                        printf(
                            '<input class="regular-text %s" id="%s" name="%s" %s type="%s" value="%s">',
                            isset($field['class']) ? $field['class'] : '',
                            $field['id'],
                            $field['id'],
                            isset($field['pattern']) ? "pattern='{$field['pattern']}'" : '',
                            $field['type'],
                            $this->value($field)
                        );
                    }

                    private function value($field)
                    {
                        global $post;
                        if (metadata_exists('post', $post->ID, $field['id'])) {
                            $value = get_post_meta($post->ID, $field['id'], true);
                        } else if (isset($field['default'])) {
                            $value = $field['default'];
                        } else {
                            return '';
                        }
                        return str_replace('\u0027', "'", $value);
                    }

                    private function checked($field)
                    {
                        global $post;
                        if (metadata_exists('post', $post->ID, $field['id'])) {
                            $value = get_post_meta($post->ID, $field['id'], true);
                            if ($value === 'on') {
                                return 'checked';
                            }
                            return '';
                        } else if (isset($field['checked'])) {
                            return 'checked';
                        }
                        return '';
                    }
                }
                new Validation;
            }
