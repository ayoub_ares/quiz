jQuery('form[name="contact-me"]').on('submit', function (e) {
    e.preventDefault();
    
    var form_data = jQuery(this).serializeArray();
    // Here we add our nonce (The one we created on our functions.php. WordPress needs this code to verify if the request comes from a valid source.
    form_data.push({
        "pic-testimo": jQuery('input#pic_testimo').prop('files')[0]
    });
    var file_data = $('input#pic_testimo').prop('files')[0];

    form_data.push('file', file_data);
    console.log(form_data);
    // Here is the ajax petition.
    jQuery.ajax({
        url: '../wp-content/plugins/module-de-testimonials/controller/save-testimo.php', // Here goes our WordPress AJAX endpoint.
        type: 'post',
        processData: false,
        contentType: false,
        data: new FormData(this),
        success: function (response) {
            // You can craft something here to handle the message return
            alert(response);
        },
        fail: function (err) {
            // You can craft something here to handle an error if something goes wrong when doing the AJAX request.
            alert("There was an error: " + err);
        }
    });

    // This return prevents the submit event to refresh the page.
    return false;
});