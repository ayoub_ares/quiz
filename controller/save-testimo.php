<?php
$path = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
require_once($path[0] . "wp-load.php");
require_once($path[0] . "wp-admin/includes/file.php");
require_once($path[0] . "wp-admin/includes/image.php");


$wordpress_upload_dir = wp_upload_dir();
// $wordpress_upload_dir['path'] is the full server path to wp-content/uploads/2017/05, for multisite works good as well
// $wordpress_upload_dir['url'] the absolute URL to the same folder, actually we do not need it, just to show the link to file
$i = 1; // number of tries when the file with the same name is already exists

//validation 
$profilepicture = $_FILES['pic-testimo'];
if (empty($_POST['title-testimo']))
	die('Titre invalid.');

if (strlen($_POST['title-testimo']) > 60)
	die('Titre max 60.');

if (empty($profilepicture['name']))
	die('File is not selected.');

if ($profilepicture['error'])
	die($profilepicture['error']);

if ($profilepicture['size'] > wp_max_upload_size())
	die('It is too large than expected.');

if (empty($_POST['subject-testimo']))
	die('Message invalid.');

if (strlen($_POST['subject-testimo']) > 300)
	die('Titre max 300.');





$new_file_path = $wordpress_upload_dir['path'] . '/' . $profilepicture['name'];
$new_file_mime = mime_content_type($profilepicture['tmp_name']);
$imgtype = array("image/png");
if (!in_array($profilepicture['type'], $imgtype))
	die('WordPress doesn\'t allow this type of uploads.');

while (file_exists($new_file_path)) {
	$i++;
	$new_file_path = $wordpress_upload_dir['path'] . '/' . $i . '_' . $profilepicture['name'];
}

// looks like everything is OK
if (move_uploaded_file($profilepicture['tmp_name'], $new_file_path)) {


	$upload_id = wp_insert_attachment(array(
		'guid'           => $new_file_path,
		'post_mime_type' => $new_file_mime,
		'post_title'     => preg_replace('/\.[^.]+$/', '', $profilepicture['name']),
		'post_content'   => '',
		'post_status'    => 'inherit'
	), $new_file_path);

	// wp_generate_attachment_metadata() won't work if you do not include this file
	require_once(ABSPATH . 'wp-admin/includes/image.php');

	// Generate and save the attachment metas into the database
	wp_update_attachment_metadata($upload_id, wp_generate_attachment_metadata($upload_id, $new_file_path));

	// Show the uploaded file in browser
	$new_post = array(
		'post_title' => $_POST['title-testimo'],
		'post_content' => $_POST['subject-testimo'],
		'post_status' => 'publish',
		'post_type'     => 'moduledetestimonials',
	);
	$post_id = wp_insert_post($new_post);
	if ($post_id) {
		echo "Post successfully published!";
		set_post_thumbnail($post_id, $upload_id);
	} else {
		echo "Something went wrong, try again.";
	}
	
}
