<h3 id="testi_mod_tittle">Testimonials</h3>
<div class="grid-container">
    <?php
    //Here we are going to fetch post named Vacancy.
    $args = array('post_type' => 'moduledetestimonials');
    $loop = new WP_Query($args);
    //WQ_QUERY is a wordpress function
    //Define loop to fetch all data under vacany post type

    while ($loop->have_posts()) :
        $loop->the_post();
        $postId = $post->ID;  //Fetch Post ID
        if (get_post_custom($post->ID)['validation_statue'][0] == "on") {
    ?>

            <div class="grid-item">
                <?php


                echo get_the_post_thumbnail($post->ID, 'thumbnail', array('class' => 'aligncenter')); ?>
                <h5><?php the_title(); //Display post title?></h5>
                <h6><?php the_content(); //Display post content ?></h6>

            </div>

    <?php }
    endwhile; ?>



</div>