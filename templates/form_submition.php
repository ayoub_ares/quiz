<?php
?>
<div class="custom-testimo">

    <div class="container">
    
        <form action="" name="contact-me" method="POST">
            <label for="title_testimo">Titre</label>
            <input type="text" id="title_testimo" name="title-testimo" placeholder="Your name..">

            <label for="pic_testimo">Photo</label>
            <input type="file" id="pic_testimo" name="pic-testimo">

            <label for="subject_testimo">Message</label>
            <textarea id="subject_testimo" name="subject-testimo" maxlength="300" style="height:200px"></textarea>

            <input type="submit" value="Submit">
        </form>
    </div>
</div>